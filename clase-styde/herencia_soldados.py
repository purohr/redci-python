class Unit():

   
    def __init__(self,name):
        self.__alive =True
        self.__name=name
        
    def getName (self):
        return self.__name
        
    def move (self, direction):
        return self.__name + ' avanza hacia a ' + direction
        
    def attack (self, opponent):
        return self.__name + ' lanza un golpe a ' + opponent        
        
class Soldier(Unit):

    def attack (self, opponent):
        return self.getName() + ' corta con una espada a ' + opponent
     
class Archer(Unit):    
    def attack (self, opponent):
        return self.getName() + ' dispara un fecha a ' + opponent


p1 = Archer('huui')
print(p1.move('el norte'))
print(p1.attack('pirlo'))


