class Persona():
    
    def __init__(self, lastName, firstName,nickName):
        self.__lastName= lastName
        self.__firstName= firstName
        self.__nickname= nickName
        
    def setFirstName(self, firstName):
        self.__firstName=firstName

    def getFirstName(self):
        return self.__firstName    

    def setNickName(self, nickName):
        if (len(nickName) > 2 and nickName != self.__lastName and nickName != self.__firstName):
            self.nickName= nickName
        
        else:
            print("ERROR: NickName es menor a dos caracteres ó es igual al nombre ó al apellido.\n",nickName)
            
    def getNickName(self):
        return self.__nickname
        
    def fullName(self):
        return self.__firstName + ' ' + self.__lastName        
        

persona1 = Persona('jaramillo', 'Diego','puroh')
persona2 = Persona('obando', 'sandro', 'cohel')

persona1.firstName= 'sebas'

print(type(persona1))

print(persona1.firstName)

#print(persona1.lastName)
print("--------------------***************--------------------")
print(persona1.fullName() +' es amigo de ' +persona2.fullName())        

print("--------------------***************--------------------")


