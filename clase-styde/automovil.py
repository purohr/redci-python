class Vehiculo():
        
        def __init__(self, marca, color, puertas):
            self.marca=marca
            self.color=color
            self.puertas=puertas
            
        def acelerar(self):
            return "Esta acelerando"
        
        def frenar(self):
            return "Esta frenando"
        
        def bocina(self):
            return "Suena la bocina"
            
        def estado(self):
            return "Marca: %s \nColor: %s \nPuertas: %d"%(self.marca, self.color, self.puertas)            
            
            
miToyota= Vehiculo('toyota', 'morado',4)
print(miToyota.estado())
            
                    
            
