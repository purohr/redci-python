from django.urls import path
from holam import views

urlpatterns = [
      path('', views.index_page, name='index'),
]
