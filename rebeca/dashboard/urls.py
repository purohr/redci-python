from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('clientes/', views.customers, name='customers'),
    path('clientes/<int:customer_id>/', views.customers_detail, name='customers.detail'),
    path('clientes/nuevo', views.customers_create, name='customers.create'),
]
