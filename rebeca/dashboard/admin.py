from django.contrib import admin

# Register your models here.
from .models import Customer, InternetService, ServiceContract, Period, PaymentOrder, Payment

admin.site.register(Customer)
admin.site.register(InternetService)
admin.site.register(ServiceContract)
admin.site.register(Period)
admin.site.register(PaymentOrder)
admin.site.register(Payment)
