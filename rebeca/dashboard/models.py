from django.db import models


class Customer(models.Model):
    first_name = models.CharField(max_length=20)
    lasyt_name = models.CharField(max_length=20)
    email = models.EmailField(unique=True, null=True)
    phone = models.BigIntegerField()

    def __str__(self):
        return self.first_name + ' ' + self.lasyt_name

    def name(self):
        return self.first_name + ' ' + self.lasyt_name


class InternetService(models.Model):
    title = models.CharField(max_length=20)
    Mbps = models.IntegerField()
    value = models.BigIntegerField()

    def __str__(self):
        return self.title


class ServiceContract(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    internet_service = models.ForeignKey(InternetService, on_delete=models.CASCADE)
    ip = models.GenericIPAddressField()
    discount = models.BigIntegerField(default=0)
    comment = models.TextField(null=True)

    def __str__(self):
        # return self.customer.name() + ' ' + str(internet_service.Mbps)
        return self.comment

    def ip(self):
        return self.ip


class Period(models.Model):
    initial_day = models.DateField()
    final_day = models.DateField()
    is_it_early_pay = models.BooleanField()


class PaymentOrder(models.Model):
    period = models.ForeignKey(Period, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    value_payment = models.BigIntegerField()


class Payment(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    date = models.DateField()
    value = models.BigIntegerField()
    discount = models.BigIntegerField()
