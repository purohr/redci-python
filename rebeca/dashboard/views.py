from django.shortcuts import render
from django.http import HttpResponse


def index(request):
    return render(request, 'dashboard/index.html')
# return HttpResponse("Estas en La pagina principal del Dashboard")


def customers(request):
    return HttpResponse("Estas en La pagina principal de Clientes")


def customers_detail(request, customer_id):
    return HttpResponse("Estas revisando el cliente %s." % customer_id)


def customers_create(request):
    return HttpResponse("Estas en la seccion de crear cliente")
