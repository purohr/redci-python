from django.shortcuts import render


def home(request):
    return render(request, 'home/index.html')


def services(request):
    return render(request, 'services/index.html')


def normativity(request):
    return render(request, 'normativity.html')


def contact(request):
    return render(request, 'contact/index.html')


def us(request):
    return render(request, 'us.html')


def test(request):
    return render(request, 'test.html')


def login(request):
    return render(request, 'login.html')
