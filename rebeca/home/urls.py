from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('servicios/', views.services, name='services'),
    path('normatividad/', views.normativity, name='normativity'),
    path('contacto/', views.contact, name='contact'),
    path('nosotros/', views.us, name='us'),
    path('test/', views.test, name='test'),
    path('IniciarSesion/', views.login, name='login'),
]
